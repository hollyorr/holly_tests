## 1. Description
I run into the following bug when:

1. Create a BigSQL sandbox
1. Install / init / start pgDevOps
1. Stop pgDevOps
1. Delete the sandbox
1. Create a new standbox
1. Install / init / start pgDevOps
1. Bug: pgDevOps will now not start
1. Old sandbox directory reappears and I don't have permissions to view

## 2. Expected Result

I should able to delete a sandbox and create a new one and start pgDevOps without issues.

## 3. Reproducing Bug

### Delete old sandboxes

	$ ls
		Applications	Downloads	Music		Sites		gis_data	pg10_test5	sub_test
		Desktop		Library		Pictures	VirtualBox VMs	microblog	pgadmin.log	t
		Documents	Movies		Public		gdal_test	pem_keys	pub_test	vagrant_repos

	$ sudo rm -rf t	
	$ sudo rm -rf pg10_test5/
	
Make sure no services are running:

	$ ps aux | grep postgres
		hollyorr          2209   0.0  0.0  2423376    208 s000  R+   12:53PM   0:00.00 grep postgres
	
	$ ps aux | grep -i crossbar
		hollyorr          2211   0.1  0.0  2454296    836 s000  S+   12:53PM   0:00.00 grep -i crossbar
	
	$ ps aux | grep -i python
		hollyorr          2213   0.0  0.0  2453272    832 s000  S+   12:53PM   0:00.00 grep -i python				
###Create a new bigsql sandbox from prod

	$ mkdir pg10_test6
	$ cd pg10_test6/
	$ python -c "$(curl -fsSL http://s3.amazonaws.com/pgcentral/install.py)"	
	$ cd bigsql
	$ ./pgc install pgdevops
	$ ./pgc init pgdevops
	$ ./pgc start pgdevops
	$ ./pgc status
		pgdevops running on port 8051
		
Verify pgDevOps Dashboard is running and then close browser:

![](images/pgdevops_works.png)
	
###Stop Services and Delete Sandbox

Stop running services (pgdevops):

	$ ./pgc stop pgdevops
		pgdevops stopping
		
	$ ./pgc status
		pgdevops stopped on port 8051

Make sure no services are running:

	$ ps aux | grep postgres
		hollyorr          2627   0.3  0.0  2423376    212 s000  R+    1:18PM   0:00.00 grep postgres
	
	$ ps aux | grep -i crossbar
		hollyorr          2629   0.0  0.0  2444056    820 s000  S+    1:18PM   0:00.00 grep -i crossbar
	
	$ ps aux | grep -i python
		hollyorr          2631   0.0  0.0  2435864    792 s000  S+    1:18PM   0:00.00 grep -i python

Delete sandbox:

	$ cd
	$ ls
		Applications	Downloads	Music		Sites		gis_data	pg10_test6	sub_test
		Desktop		Library		Pictures	VirtualBox VMs	microblog	pgadmin.log	vagrant_repos
		Documents	Movies		Public		gdal_test	pem_keys	pub_test
	$ rm -rf pg10_test6
	$ ls
		Applications	Downloads	Music		Sites		gis_data	pgadmin.log	vagrant_repos
		Desktop		Library		Pictures	VirtualBox VMs	microblog	pub_test
		Documents	Movies		Public		gdal_test	pem_keys	sub_test
		
###Create new Sandbox and Start New pgDevOps Instance

	$ mkdir pg10_test7
	$ cd pg10_test7
	$ python -c "$(curl -fsSL http://s3.amazonaws.com/pgcentral/install.py)"
	$ cd bigsql
	$ ./pgc install pgdevops
	$ ./pgc init pgdevops
	$ ./pgc status
		pgdevops stopped on port 8051
	$ ./pgc start pgdevops
		pgdevops starting on port 8051
		
Status shows pgdevops will not start
	$ ./pgc status
		pgdevops stopped on port 8051

pg10_test6 has been recreated somehow:
		
	$ cd ../..
	$ ls
		Applications	Downloads	Music		Sites		gis_data	pg10_test6	pub_test
		Desktop		Library		Pictures	VirtualBox VMs	microblog	pg10_test7	sub_test
		Documents	Movies		Public		gdal_test	pem_keys	pgadmin.log	vagrant_repos
		
Try to see what artifacts are left in old installation:

	$ cd pg10_test6/bigsql
	$ ls
		pgdevops
	$ cd pgdevops
	$ ls
		logs
	$ cd logs
		-bash: cd: logs: Permission denied
		
Navigating in finder gets permission denies message:
![](images/finder.png)

Make sure no services are running:

	$ ps aux | grep postgres
		hollyorr          2957   0.0  0.0  2444056    808 s000  S+    1:51PM   0:00.00 grep postgres
	
	$ ps aux | grep -i crossbar
		hollyorr          2959   0.0  0.0  2423380    324 s000  R+    1:51PM   0:00.00 grep -i crossbar
	
	$ ps aux | grep -i python
		hollyorr          2961   0.0  0.0  2434840    780 s000  S+    1:51PM   0:00.00 grep -i python

In finder deleted logs directory by entering admin password for machine. Then deleted pg10_test6 directory.

	$ ls
		Applications	Downloads	Music		Sites		gis_data	pg10_test7	sub_test
		Desktop		Library		Pictures	VirtualBox VMs	microblog	pgadmin.log	vagrant_repos
		Documents	Movies		Public		gdal_test	pem_keys	pub_test

pgDevOps still fails to start:

	$ ./pgc start pgdevops
		pgdevops starting on port 8051
	$ ./pgc status
		pgdevops stopped on port 8051
