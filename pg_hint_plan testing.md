##Testing:

* Install 10.0.1.118 BigSQL Linux ONLY - Centos72

		python -c "$(curl -fsSL http://s3.amazonaws.com/pgcentral/install.py)"
		./pgc set GLOBAL REPO http://10.0.1.118
		./pgc update
		./pgc install pg96
		./pgc start
		./pgc status
		
Install pg\_hint\_plan

	[vagrant@centos72-88 bigsql]$ ./pgc install hintplan-pg96
	  ['hintplan-pg96']
	Get:1 http://10.0.1.118 hintplan-pg96-1.2.2-1-linux64
	 Unpacking hintplan-pg96-1.2.2-1-linux64.tar.bz2
	 	 
##Links:

official documentation:

* [http://pghintplan.osdn.jp/](http://pghintplan.osdn.jp/)
* [http://pghintplan.osdn.jp/pg_hint_plan.html](http://pghintplan.osdn.jp/pg_hint_plan.html)

Community take on it:

* [https://wiki.postgresql.org/wiki/OptimizerHintsDiscussion
](https://wiki.postgresql.org/wiki/OptimizerHintsDiscussion)

Blog posts about it:

* [https://dbmstuning.wordpress.com/2013/07/08/you-can-use-hint-using-pg_plan_hint-for-postgresql/](https://dbmstuning.wordpress.com/2013/07/08/you-can-use-hint-using-pg_plan_hint-for-postgresql/)


Oracle Documentation:

[https://docs.oracle.com/cd/B10501_01/server.920/a96533/hintsref.htm#8326](https://docs.oracle.com/cd/B10501_01/server.920/a96533/hintsref.htm#8326)

http://allthingsoracle.com/a-beginners-guide-to-optimizer-hints/