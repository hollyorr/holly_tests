##Initial Set-Up

####Installed pg96 on Centos71 vm  

	./pgc info	
	######################################################################
		#     PGC: v3.2.4  /home/vagrant/bigsql
		#     User & Host: vagrant  centos71-92 127.0.0.1
		#        Platform: el7 | CentOS Linux 7.1.1503 (Core)
		#          Python: /usr/bin/python | v2.7.5 | pip vNone
		#        Hardware: 1.8 GB, 2 x Intel Core i5-3210M @ 2.50GHz
		#        Repo URL: http://s3.amazonaws.com/pgcentral
		#     Last Update: 2017-07-26 15:03:05
	######################################################################

	./pgc list
		Category     | Component           | Version  | Stage | ReleaseDt  | Status    | Cur? | Updates
		PostgreSQL     pg96                  9.6.3-2            2017-07-10   Installed   1

####Installed pg96 and pgDevOps from test server on my Mac

	
	./pgc info	
	######################################################################
	#             PGC: v3.2.6  /Users/hollyorr/pgdg_july/bigsql
	#     User & Host: hollyorr  Hollys-MBP 192.168.1.7
	#        Platform: osx | Mac OS X 10.11.6
	#          Python: pip v9.0.1 | v2.7.10 | /usr/bin/python
	#        Hardware: 8 GB, 2 x Intel Core i5-3210M @ 2.50GHz
	#        Repo URL: http://10.0.1.118
	#     Last Update: 2017-07-26 10:27:26
	######################################################################
	
	./pgc list
	Category     | Component           | Version  | ReleaseDt  | Status    | Updates
	PostgreSQL     pg96                  9.6.3-2    2017-07-10   Installed
	Servers        pgdevops              1.6-1      2017-07-28   Installed
	Applications   pgadmin3              1.23.0b    2017-06-08   Installed
	

####Created private network in vagrant file

Added the last line to create private network on 192.168.50.51:

	Vagrant.configure(2) do |config|
	
	  config.vm.box="http://bigsql-mirror:8000/vms/openscg-centos71-BASE_x86_64.box"
	  ostype="centos71"
	  #ostype="centos71"
	  #config.vm.box = "openscg/centos71"
	  #config.vm.box_url = "http://bigsql-mirror/vms/repo/vms/centos71/openscg-centos71-6.0.box"
	  config.vm.network "private_network", ip: "192.168.50.51"
	  
* Restart vagrant box
* Restart pg96 

##Turn off firewall

Stop firewalld and check status:

	[vagrant@centos71-5 bigsql]$ sudo systemctl stop firewalld
	[vagrant@centos71-92 bigsql]$ sudo systemctl status firewalld
	firewalld.service
	   Loaded: masked (/dev/null)
	   Active: inactive (dead)

####Set the SELinux to disabled

[SELinux](https://en.wikipedia.org/wiki/Security-Enhanced_Linux) 

	[vagrant@centos71-92 bigsql]$ sudo vi /etc/selinux/config

change to disabled:

	# This file controls the state of SELinux on the system.
	# SELINUX= can take one of these three values:
	#     enforcing - SELinux security policy is enforced.
	#     permissive - SELinux prints warnings instead of enforcing.
	#     disabled - No SELinux policy is loaded.
	SELINUX=disabled
	# SELINUXTYPE= can take one of three two values:
	#     targeted - Targeted processes are protected,
	#     minimum - Modification of targeted policy. Only selected processes are protected.
	#     mls - Multi Level Security protection.
	SELINUXTYPE=targeted

If want to disable SelLinux at runtime:

	[vagrant@centos71-92 bigsql]$ sudo setenforce 0
		setenforce: SELinux is disabled
		
To check current status:

	sudo sestatus
	
* Restart vagrant box
* Restart pg96 

##Add SSH Host

####pgDevOps on Mac Localhost Server Manager Dashboard:

![](images/devops1.png)

* Go to Add >> Manage(d) ssh host

![](images/ssh_host.png)

* Host IP
* user: vagrant
* pw: vagrant

####click associate:

![](images/discover.png)
![](images/update1.png)
![](images/update2.png)

####Go to package manager

![](images/package.png)

####Go to pgAdmin4

Server is added:

![](images/pgadmin4_servers.png)

When try to connect to linux server, get this error:

![](images/pgadmin_connect_error.png)

##Troubleshooting

Here are some things I did to troubleshoot issue:

####Try to connect with psql

run psql connecting to server:
	
	Hollys-MBP:pg96 hollyorr$ psql -h 192.168.50.51
	psql: could not connect to server: Connection refused
		Is the server running on host "192.168.50.51" and accepting
		TCP/IP connections on port 5432?

try again with more verbose options:

	Hollys-MBP:pg96 hollyorr$ psql -h 192.168.50.51 -U postgres -d postgres
	psql: could not connect to server: Connection refused
		Is the server running on host "192.168.50.51" and accepting
		TCP/IP connections on port 5432?

####Ping server

Pinging works:

	Hollys-MBP:pg96 hollyorr$ ping 192.168.50.51
	PING 192.168.50.51 (192.168.50.51): 56 data bytes
	64 bytes from 192.168.50.51: icmp_seq=0 ttl=64 time=0.553 ms
	64 bytes from 192.168.50.51: icmp_seq=1 ttl=64 time=0.459 ms
	64 bytes from 192.168.50.51: icmp_seq=2 ttl=64 time=0.318 ms
	64 bytes from 192.168.50.51: icmp_seq=3 ttl=64 time=0.481 ms
	64 bytes from 192.168.50.51: icmp_seq=4 ttl=64 time=0.519 ms
	64 bytes from 192.168.50.51: icmp_seq=5 ttl=64 time=0.507 ms
	64 bytes from 192.168.50.51: icmp_seq=6 ttl=64 time=0.552 ms
	^C
	--- 192.168.50.51 ping statistics ---
	7 packets transmitted, 7 packets received, 0.0% packet loss
	round-trip min/avg/max/stddev = 0.318/0.484/0.553/0.075 ms

####Make sure $PGPORT is set to 5432

	[vagrant@centos71-3 pg96]$ printenv PGPORT
	5432

####Run socket statistics (ss) command

		[vagrant@centos71-3 pg96]$ ss -lntpu
		Netid  State      Recv-Q Send-Q   Local Address:Port     Peer Address:Port
		tcp    UNCONN     0      0                    *:68                  *:*
		tcp    UNCONN     0      0                    *:29822               *:*
		tcp    UNCONN     0      0                   :::42434              :::*
		tcp    LISTEN     0      128                  *:22                  *:*
		tcp    LISTEN     0      128                  *:5432                *:*      users:(("postgres",4468,3))
		tcp    LISTEN     0      100          127.0.0.1:25                  *:*
		tcp    LISTEN     0      128                 :::22                 :::*
		tcp    LISTEN     0      128                 :::5432               :::*      users:(("postgres",4468,4))
		tcp    LISTEN     0      100                ::1:25                 :::*

####Run ifconfig

	[vagrant@centos71-3 pg96]$ ifconfig
	
	enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
	        inet 10.0.2.15  netmask 255.255.255.0  broadcast 10.0.2.255
	        inet6 fe80::a00:27ff:fef6:b007  prefixlen 64  scopeid 0x20<link>
	        ether 08:00:27:f6:b0:07  txqueuelen 1000  (Ethernet)
	        RX packets 1518  bytes 194674 (190.1 KiB)
	        RX errors 0  dropped 0  overruns 0  frame 0
	        TX packets 904  bytes 112424 (109.7 KiB)
	        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
	
	enp0s8: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
	        inet 192.168.50.51  netmask 255.255.255.0  broadcast 192.168.50.255
	        inet6 fe80::a00:27ff:fe0c:de40  prefixlen 64  scopeid 0x20<link>
	        ether 08:00:27:0c:de:40  txqueuelen 1000  (Ethernet)
	        RX packets 727  bytes 86122 (84.1 KiB)
	        RX errors 0  dropped 0  overruns 0  frame 0
	        TX packets 544  bytes 167773 (163.8 KiB)
	        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
	
	lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
	        inet 127.0.0.1  netmask 255.0.0.0
	        inet6 ::1  prefixlen 128  scopeid 0x10<host>
	        loop  txqueuelen 0  (Local Loopback)
	        RX packets 638  bytes 239686 (234.0 KiB)
	        RX errors 0  dropped 0  overruns 0  frame 0
	        TX packets 638  bytes 239686 (234.0 KiB)
	        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0	

####Restart Linux vm

	Hollys-MBP:centos71 hollyorr$ vagrant halt
		==> centos71: Attempting graceful shutdown of VM...
	Hollys-MBP:centos71 hollyorr$ vagrant up



