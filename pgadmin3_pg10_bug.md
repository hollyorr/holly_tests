## Downloading to sandbox on mac from production throws errors:

	
	Hollys-MacBook-Pro:~ hollyorr$ mkdir pg10_pgadmin3
	Hollys-MacBook-Pro:~ hollyorr$ cd pg10_pgadmin3
	Hollys-MacBook-Pro:pg10_pgadmin3 hollyorr$ python -c "$(curl -fsSL http://s3.amazonaws.com/pgcentral/install.py)"
	
	Downloading BigSQL PGC 3.1.7 ...
	
	Unpacking ...
	
	Cleaning up
	
	Setting REPO to http://s3.amazonaws.com/pgcentral
	
	Updating Metadata
	 
	
	BigSQL PGC installed.  Try 'bigsql/pgc help' to get started.
	
	Hollys-MacBook-Pro:pg10_pgadmin3 hollyorr$ cd bigsql
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc info
	###########################################################################
	#             PGC: v3.1.7  /Users/hollyorr/pg10_pgadmin3/bigsql
	#     User & Host: hollyorr  Hollys-MacBook-Pro 10.0.1.2
	#        Platform: osx | Mac OS X 10.11.6 | python 2.7.10 | pip 9.0.1
	#        Hardware: 8 GB, 2 x Intel Core i5-3210M @ 2.50GHz
	#        Repo URL: http://s3.amazonaws.com/pgcentral
	# Last Update UTC: 2017-06-20 13:58:39
	###########################################################################
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc list
	Category     | Component | Version  | Stage | ReleaseDt  | Status | Cur? | Updates
	PostgreSQL     pg92        9.2.21-1           2017-05-11            1               
	PostgreSQL     pg93        9.3.17-1           2017-05-11            1               
	PostgreSQL     pg94        9.4.12-1           2017-05-11            1               
	PostgreSQL     pg95        9.5.7-1            2017-05-11            1               
	PostgreSQL     pg96        9.6.3-1            2017-05-11            1               
	Servers        pgdevops    1.4-1              2017-05-18            1               
	Applications   backrest    1.18               2017-05-18            1               
	Applications   ora2pg      18.1               2017-03-23            1               
	Applications   pgadmin3    1.23.0b            2017-06-08            1               
	Applications   pgbadger    9.1                2017-02-09            1               
	Frameworks     java8       8u121              2017-02-09            1               
	
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc install pg10
	  ['pg10']
	Get:1 http://s3.amazonaws.com/pgcentral pg10-10beta1-1-osx64
	  Unpacking pg10-10beta1-1-osx64.tar.bz2
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc start
	 
	## Initializing pg10 #######################
	 
	Superuser Password [password]: 
	Confirm Password: 
	Setting directory and file permissions.
	 
	Initializing Postgres DB at:
	   -D "/Users/hollyorr/pg10_pgadmin3/bigsql/data/pg10" 
	 
	Using PostgreSQL Port 5432
	 
	Password securely remembered in the file: /Users/hollyorr/.pgpass
	 
	to load this postgres into your environment, source the env file: 
	    /Users/hollyorr/pg10_pgadmin3/bigsql/pg10/pg10.env
	 
	pg10 starting on port 5432
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc install pgadmin3
	  ['pgadmin3']
	Get:1 http://s3.amazonaws.com/pgcentral pgadmin3-1.23.0b-osx64
	  Unpacking pgadmin3-1.23.0b-osx64.tar.bz2
	ln: /Applications/pgAdmin3 LTS by BigSQL.app: File exists
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc start pgadmin3
	Starting pgAdmin III...
	  logging to: /var/folders/8h/ypgsr0k16gzbd8435fmc84sw0000gn/T/pgadmin3.log
	Hollys-MacBook-Pro:bigsql hollyorr$ .pgc update
	-bash: .pgc: command not found
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc update
	Retrieving the remote list of latest component versions (versions.sql) ...
	Validating checksum file...
	Updating local repository with remote entries...
	'hub' is v3.1.7
	 
	No updates available.
	
	New components released in the last 30 days.
	Category     | Component         | Version | Stage | ReleaseDt  | Status    | Cur? | Updates
	Extensions     pldebugger96-pg10   9.6.0-2           2017-06-08               1               
	Extensions     plprofiler3-pg10    3.2-1             2017-06-08               1               
	Applications   pgadmin3            1.23.0b           2017-06-08   Installed   1               
	
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc stop
	pg10 stopping
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc upgrade pg10
	Nothing to upgrade.
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc upgrade pgadmin3
	Nothing to upgrade.
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc start pg10
	pg10 starting on port 5432
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc start pgadmin3
	Starting pgAdmin III...
	  logging to: /var/folders/8h/ypgsr0k16gzbd8435fmc84sw0000gn/T/pgadmin3.log
	Hollys-MacBook-Pro:bigsql hollyorr$ 
	
![](images/pgadmin1.png) 
![](images/pgadmin2.png) 
![](images/pgadmin3.png) 
![](images/pgadmin4.png) 
![](images/pgadmin5.png) 
![](images/pgadmin6.png) 
![](images/pgadmin7.png) 
![](images/pgadmin8.png) 
![](images/pgadmin9.png) 
![](images/pgadmin10.png) 

## Updating/upgrading pg10 from mirror fixes issue - no errors thrown when connecting to pg10 instance from pgadmin3:

	
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc update
	Retrieving the remote list of latest component versions (versions.sql) ...
	Validating checksum file...
	Updating local repository with remote entries...
	Automatic updating 'hub' from v3.1.7 to v3.2.1.
	 
	upgrading hub from (3.1.7) to (3.2.1)
	Get:1 http://10.0.1.118 hub-3.2.1
	  Unpacking hub(3.2.1) over (3.1.7)
	
	Running update-hub from v3.1.7 to v3.2.1
	 
	Goodbye.
	Category     | Component         | Version   | Stage | ReleaseDt  | Status    | Cur? | Updates  
	PostgreSQL     pg10                10beta1-1   test    2017-05-18   Installed   0      10beta1-2  
	PostgreSQL     pg92                9.2.21-2            2017-06-23               1                 
	PostgreSQL     pg93                9.3.17-2            2017-06-23               1                 
	PostgreSQL     pg94                9.4.12-2            2017-06-23               1                 
	PostgreSQL     pg95                9.5.7-2             2017-06-23               1                 
	PostgreSQL     pg96                9.6.3-2             2017-06-23               1                 
	Extensions     orafce3-pg10        3.4.0-1             2017-06-08               1                 
	Extensions     pldebugger96-pg10   9.6.0-2             2017-06-08               1                 
	Extensions     plprofiler3-pg10    3.2-1               2017-06-08               1                 
	Servers        pgdevops            1.5-3               2017-06-23               1                 
	Applications   backrest            1.18                2017-05-18               1                 
	Applications   ora2pg              18.1                2017-03-23               1                 
	Applications   pgadmin3            1.23.0b             2017-06-08   Installed   1                 
	Applications   pgbadger            9.1                 2017-02-09               1                 
	Frameworks     java8               8u121               2017-02-09               1                 
	
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc upgrade pg10
	pg10 stopping
	upgrading pg10 from (10beta1-1) to (10beta1-2)
	Get:1 http://10.0.1.118 pg10-10beta1-2-osx64
	 Unpacking pg10(10beta1-2) over (10beta1-1)
	pg10 starting on port 5432
	Hollys-MacBook-Pro:bigsql hollyorr$ ./pgc start pgadmin3
	Starting pgAdmin III...
	  logging to: /var/folders/8h/ypgsr0k16gzbd8435fmc84sw0000gn/T/pgadmin3.log
