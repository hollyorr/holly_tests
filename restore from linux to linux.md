##Install BigSQL on Linux vm
	
* Install 10.0.1.118 BigSQL

		python -c "$(curl -fsSL http://s3.amazonaws.com/pgcentral/install.py)"
		./pgc set GLOBAL REPO http://10.0.1.118
		./pgc update
		./pgc install pg96
		./pgc start
		./pgc status


## Remote Linux to Linux test

	COPY medallion_drivers(license_num,name,type,exp_date,update_date,update_time) 
		FROM '/vagrant/medallions.csv' DELIMITER ',' CSV HEADER;