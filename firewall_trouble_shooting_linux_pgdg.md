	[vagrant@centos71-5 bigsql]$ ss -lntpu
	Netid  State      Recv-Q Send-Q     Local Address:Port       Peer Address:Port 
	tcp    UNCONN     0      0                      *:68                    *:*     
	tcp    UNCONN     0      0                      *:27769                 *:*     
	tcp    UNCONN     0      0                     :::42786                :::*     
	tcp    LISTEN     0      128                    *:22                    *:*     
	tcp    LISTEN     0      128                    *:5432                  *:*      users:(("postgres",15229,3))
	tcp    LISTEN     0      100            127.0.0.1:25                    *:*     
	tcp    LISTEN     0      128                   :::22                   :::*     
	tcp    LISTEN     0      128                   :::5432                 :::*      users:(("postgres",15229,4))
	tcp    LISTEN     0      100                  ::1:25                   :::*     
	[vagrant@centos71-5 bigsql]$ ifconfig
	enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
	        inet 10.0.2.15  netmask 255.255.255.0  broadcast 10.0.2.255
	        inet6 fe80::a00:27ff:fef6:b007  prefixlen 64  scopeid 0x20<link>
	        ether 08:00:27:f6:b0:07  txqueuelen 1000  (Ethernet)
	        RX packets 44027  bytes 3216468 (3.0 MiB)
	        RX errors 0  dropped 0  overruns 0  frame 0
	        TX packets 41328  bytes 3757982 (3.5 MiB)
	        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
	
	enp0s8: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
	        inet 192.168.50.51  netmask 255.255.255.0  broadcast 192.168.50.255
	        inet6 fe80::a00:27ff:fe0c:de40  prefixlen 64  scopeid 0x20<link>
	        ether 08:00:27:0c:de:40  txqueuelen 1000  (Ethernet)
	        RX packets 12194  bytes 1907302 (1.8 MiB)
	        RX errors 0  dropped 0  overruns 0  frame 0
	        TX packets 10671  bytes 3490147 (3.3 MiB)
	        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
	
	lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
	        inet 127.0.0.1  netmask 255.0.0.0
	        inet6 ::1  prefixlen 128  scopeid 0x10<host>
	        loop  txqueuelen 0  (Local Loopback)
	        RX packets 9390  bytes 2662322 (2.5 MiB)
	        RX errors 0  dropped 0  overruns 0  frame 0
	        TX packets 9390  bytes 2662322 (2.5 MiB)
	        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
	
	[vagrant@centos71-5 bigsql]$ systemctl stop firewalld
	Failed to issue method call: Access denied
	[vagrant@centos71-5 bigsql]$ sudo systemctl stop firewalld
	[vagrant@centos71-5 bigsql]$ sudo sytstemctl status
	sudo: sytstemctl: command not found
	[vagrant@centos71-5 bigsql]$ sudo systemctl status firewalld
	firewalld.service
	   Loaded: masked (/dev/null)
	   Active: inactive (dead)
	
	Jul 25 12:29:27 centos71-5 systemd[1]: Stopped firewalld.service.
	[vagrant@centos71-5 bigsql]$ sudo vi /etc/selinux/config
	
change to disabled

	# This file controls the state of SELinux on the system.
	# SELINUX= can take one of these three values:
	#     enforcing - SELinux security policy is enforced.
	#     permissive - SELinux prints warnings instead of enforcing.
	#     disabled - No SELinux policy is loaded.
	SELINUX=disabled
	# SELINUXTYPE= can take one of three two values:
	#     targeted - Targeted processes are protected,
	#     minimum - Modification of targeted policy. Only selected processes are protected.
	#     mls - Multi Level Security protection.
	SELINUXTYPE=targeted
	
	
	~
	~
	~
	~
	~
	~
	~
	~
	~
	"/etc/selinux/config" 14L, 545C
	
continue troubleshooting
	
	[vagrant@centos71-5 bigsql]$ ls
	conf  data  hub  logs  pg96  pgc
	[vagrant@centos71-5 bigsql]$ sudo setenforce 0
	[vagrant@centos71-5 bigsql]$ ls
	conf  data  hub  logs  pg96  pgc
	[vagrant@centos71-5 bigsql]$ cd data 
	[vagrant@centos71-5 data]$ ls
	logs  pg96
	[vagrant@centos71-5 data]$ cd pg96
	[vagrant@centos71-5 pg96]$ ks
	-bash: ks: command not found
	[vagrant@centos71-5 pg96]$ ls
	base     pg_commit_ts  pg_ident.conf  pg_notify    pg_snapshots  pg_subtrans  PG_VERSION            postgresql.conf
	global   pg_dynshmem   pg_logical     pg_replslot  pg_stat       pg_tblspc    pg_xlog               postmaster.opts
	pg_clog  pg_hba.conf   pg_multixact   pg_serial    pg_stat_tmp   pg_twophase  postgresql.auto.conf  postmaster.pid
	[vagrant@centos71-5 pg96]$ vi pg_hba.conf
	[vagrant@centos71-5 pg96]$ cd ../..
	[vagrant@centos71-5 bigsql]$ ls
	conf  data  hub  logs  pg96  pgc
	[vagrant@centos71-5 bigsql]$ cd data/pg96
	[vagrant@centos71-5 pg96]$ ls
	base     pg_commit_ts  pg_ident.conf  pg_notify    pg_snapshots  pg_subtrans  PG_VERSION            postgresql.conf
	global   pg_dynshmem   pg_logical     pg_replslot  pg_stat       pg_tblspc    pg_xlog               postmaster.opts
	pg_clog  pg_hba.conf   pg_multixact   pg_serial    pg_stat_tmp   pg_twophase  postgresql.auto.conf  postmaster.pid
	[vagrant@centos71-5 pg96]$ cat postmaster.pid
	15229
	/home/vagrant/postgis_gdal/bigsql/data/pg96
	1500922872
	5432
	/tmp
	*
	  5432001     32768
	[vagrant@centos71-5 pg96]$ kill -HUP 15229
	[vagrant@centos71-5 pg96]$ ls
	base     pg_commit_ts  pg_ident.conf  pg_notify    pg_snapshots  pg_subtrans  PG_VERSION            postgresql.conf
	global   pg_dynshmem   pg_logical     pg_replslot  pg_stat       pg_tblspc    pg_xlog               postmaster.opts
	pg_clog  pg_hba.conf   pg_multixact   pg_serial    pg_stat_tmp   pg_twophase  postgresql.auto.conf  postmaster.pid
	[vagrant@centos71-5 pg96]$ cd ../..
	[vagrant@centos71-5 bigsql]$ ls
	conf  data  hub  logs  pg96  pgc
	[vagrant@centos71-5 bigsql]$ ./pgc restart pg96
	pg96 stopping
	pg96 starting on port 5432
	[vagrant@centos71-5 bigsql]$ cd data
	[vagrant@centos71-5 data]$ ls
	logs  pg96
	[vagrant@centos71-5 data]$ cd pg96
	[vagrant@centos71-5 pg96]$ ls
	base     pg_commit_ts  pg_ident.conf  pg_notify    pg_snapshots  pg_subtrans  PG_VERSION            postgresql.conf
	global   pg_dynshmem   pg_logical     pg_replslot  pg_stat       pg_tblspc    pg_xlog               postmaster.opts
	pg_clog  pg_hba.conf   pg_multixact   pg_serial    pg_stat_tmp   pg_twophase  postgresql.auto.conf  postmaster.pid
	[vagrant@centos71-5 pg96]$ vi pg_hba.conf
	[vagrant@centos71-5 pg96]$ cd ../..
	[vagrant@centos71-5 bigsql]$ ls
	conf  data  hub  logs  pg96  pgc
	[vagrant@centos71-5 bigsql]$ ./pgc restart pg96
	pg96 stopping
	pg96 starting on port 5432