## Discover and Manage pgdg 

###Install BigSQL on Local Mac
	
* Install 10.0.1.118 BigSQL/pgDevOps on Mac

		python -c "$(curl -fsSL http://s3.amazonaws.com/pgcentral/install.py)"
		./pgc set GLOBAL REPO http://10.0.1.118
		./pgc update
		./pgc install pg96
		./pgc start
		./pgc install pgdevops
		./pgc start pgdevops
		./pgc status	

	
* Launch http://localhost:8051/  

* Go to Settings >> 
  
	* PGDG Linux Repository >> Enable  
	* Multi Server Manager >> Enable  

![](images/enable.png)


###Setup private network on vagrant for testing

Here is what you need for vagrant private networking:

	config.vm.network "private_network", ip: "xx.xx.xx.xx"
	ex. config.vm.network "private_network", ip: "192.168.20.2"

Don't name use a subnet with 1 at the end (192.168.20.1) because that it the host's gateway address (and it wont work).

So, for my testing vagrant a	dd this line right under config.vm.box_url in your Vagrantfile:

	config.vm.network “private_network”, ip: “192.168.50.50"
	
###Installing pgdg on Centos72 vms:

Scott's screencast on install pgdg with yum:  
[https://asciinema.org/a/y0NoFONyzXRiYY2Bm0hDePfde?t=1](https://asciinema.org/a/y0NoFONyzXRiYY2Bm0hDePfde?t=1)
	
	ls -alh		
	sudo yum install -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm
	sudo yum install postgresql96
	sudo yum install postgresql96-server
	sudo /usr/pgsql-9.6/bin/postgresql96-setup initdb
	sudo service postgresql-9.6 start
	ps aux | grep -i postgres
	sudo su - postgres
	psql

###Add SSH Host in Server Manager

Go to Server Manager >>

* Add >> SSH HOST

![](images/add_ssh.png)

user: vagrant  
password: vagrant  

![](images/pgc_home.png)

Nothing shows in the right panel. Is it supposed to?

![](images/added_host.png)


![](images/package_pgdg.png)


###Now run ./pgc discover command in Linux box

	[vagrant@centos72-52 ~]$ ls
	bigsql
	
	[vagrant@centos72-52 ~]$ cd bigsql
	[vagrant@centos72-52 bigsql]$ ls
	conf  data  hub  logs  pgc
	
	[vagrant@centos72-52 bigsql]$ ./pgc discover
	Discover pgdg v9.6
	Do you want to install pgc controller for existing pgdg96 instance:(y/n)y
	Installing pgc controller for existing pgdg96 instance.
	Discover pgdg v9.5
	  not found
	Discover pgdg v9.4
	  not found
	Discover pgdg v9.3
	  not found
	Discover pgdg v9.2
	  not found

Going back to BigSQL tab in Package Manager shows this. Is this the right icon for the pgdg version?:

![](images/discover_manager.png)

Now, when I go to the Server Manager dashboard, pgdg shows up (after I have used the discover command to install pgc:

![](images/server_pgdg_discover.png)

Stop and start works:

	[vagrant@centos72-52 bigsql]$ ./pgc status
	pgdg96 stopped on port 5432
	
![](images/pgdg_stop.png)
	
	
![](images/pgdg_start.png)

####pgdg management from package manager under bigsql tab

Cannot install postgis on pgdg through package manager:

![](images/postgis_fail.png)

nor partman:

![](images/partman_fail.png)
 
 nor ora_fce:

![](images/orafce_fail.png)

####pgdg management from package manager under pgdg tab

![](images/partman_success_pgdg_screen.png)

Error after tryin to view package manager for pgdg repository

![](images/json_error.png)


	[vagrant@centos72-52 bigsql]$ ./pgc list
	Category     | Component   | Version  | ReleaseDt  | Status    | Updates
	PostgreSQL     pg92          9.2.21-2   2017-07-10
	PostgreSQL     pg93          9.3.17-2   2017-07-10
	PostgreSQL     pg94          9.4.12-2   2017-07-10
	PostgreSQL     pg95          9.5.7-2    2017-07-10
	PostgreSQL     pg96          9.6.3-2    2017-07-10
	PostgreSQL     pgdg96        9.6-1      2016-09-29   Installed
	Servers        pgbouncer17   1.7.2-1b   2017-02-09
	Servers        pgdevops      1.6-1      2017-07-28
	Applications   backrest      1.20       2017-07-28
	Applications   ora2pg        18.1       2017-03-23
	Applications   patroni12     1.2.5      2017-05-18
	Applications   pgbadger      9.1        2017-02-09
	Frameworks     java8         8u121      2017-02-09

### Remove pgdg

	[vagrant@centos72-52 bigsql]$ ls
	conf  data  hub  logs  pgc  pgdg96
	
	[vagrant@centos72-52 bigsql]$ ./pgc remove pgdg96
	$ sudo systemctl stop postgresql-9.6
	pgdg96 removing
	
	[vagrant@centos72-52 bigsql]$ ls
	conf  data  hub  logs  pgc
	
	[vagrant@centos72-52 bigsql]$ ps aux | grep -i postgres
	vagrant  26732  0.0  0.0 112648   992 pts/0    R+   04:58   0:00 grep --color=auto -i postgres
	
When I reinstall pgdg through yum:

	[vagrant@centos72-52 ~]$ ls -alh
	total 24K
	drwx------. 4 vagrant vagrant 4.0K Jul 28 02:48 .
	drwxr-xr-x. 3 root    root      20 Oct 17  2016 ..
	-rw-------  1 vagrant vagrant  342 Jul 28 02:18 .bash_history
	-rw-r--r--. 1 vagrant vagrant   18 Nov 20  2015 .bash_logout
	-rw-r--r--. 1 vagrant vagrant  193 Nov 20  2015 .bash_profile
	-rw-r--r--. 1 vagrant vagrant  231 Nov 20  2015 .bashrc
	drwxrwxr-x  6 vagrant vagrant   59 Jul 28 04:41 bigsql
	drwx------  2 vagrant vagrant   28 Jul 28 01:25 .ssh
	-rw-r--r--  1 vagrant vagrant    6 Oct 17  2016 .vbox_version
	
	[vagrant@centos72-52 ~]$ sudo yum install -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm
	Loaded plugins: fastestmirror
	pgdg-centos96-9.6-3.noarch.rpm                   | 4.7 kB     00:00
	Examining /var/tmp/yum-root-XqzbaK/pgdg-centos96-9.6-3.noarch.rpm: pgdg-centos96-9.6-3.noarch
	/var/tmp/yum-root-XqzbaK/pgdg-centos96-9.6-3.noarch.rpm: does not update installed package.
	Error: Nothing to do
	
	[vagrant@centos72-52 ~]$ sudo yum install postgresql96
	Loaded plugins: fastestmirror
	Loading mirror speeds from cached hostfile
	 * base: mirror.5ninesolutions.com
	 * extras: centos.mbni.med.umich.edu
	 * updates: mirror.scalabledns.com
	Package postgresql96-9.6.3-1PGDG.rhel7.x86_64 already installed and latest version
	Nothing to do
	[vagrant@centos72-52 ~]$ sudo yum install postgresql96-server
	Loaded plugins: fastestmirror
	Loading mirror speeds from cached hostfile
	 * base: mirror.5ninesolutions.com
	 * extras: centos.mbni.med.umich.edu
	 * updates: mirror.scalabledns.com
	Package postgresql96-server-9.6.3-1PGDG.rhel7.x86_64 already installed and latest version
	Nothing to do
	
	[vagrant@centos72-52 ~]$ sudo /usr/pgsql-9.6/bin/postgresql96-setup initdb
	Data directory is not empty!
	
	[vagrant@centos72-52 ~]$ sudo service postgresql-9.6 start
	Redirecting to /bin/systemctl start  postgresql-9.6.service
	[vagrant@centos72-52 ~]$ ps aux | grep -i postgres
	postgres 26796  0.3  0.8 355440 15236 ?        Ss   05:22   0:00 /usr/pgsql-9.6/bin/postmaster -D /var/lib/pgsql/9.6/data/
	postgres 26800  0.0  0.0 210444  1532 ?        Ss   05:22   0:00 postgres: logger process
	postgres 26802  0.0  0.0 355440  1648 ?        Ss   05:22   0:00 postgres: checkpointer process
	postgres 26803  0.0  0.0 355440  1884 ?        Ss   05:22   0:00 postgres: writer process
	postgres 26804  0.0  0.0 355440  1648 ?        Ss   05:22   0:00 postgres: wal writer process
	postgres 26805  0.0  0.1 355884  2728 ?        Ss   05:22   0:00 postgres: autovacuum launcher process
	postgres 26806  0.0  0.1 210440  1888 ?        Ss   05:22   0:00 postgres: stats collector process
	vagrant  26808  0.0  0.0 112648   992 pts/0    R+   05:23   0:00 grep --color=auto -i postgres
	
	[vagrant@centos72-52 ~]$ sudo su - postgres
	Last login: Fri Jul 28 02:45:39 JST 2017 on pts/0
	-bash-4.2$ psql
	psql (9.6.3)
	Type "help" for help.
	
And my table I created before running remove command still exists:
	
	postgres-# \dt *.*
	                        List of relations
	       Schema       |          Name           | Type  |  Owner
	--------------------+-------------------------+-------+----------
	 public             | medallion_drivers       | table | postgres

Had to delete bigsql directory and then run:

	sudo yum erase postgresql96*

###Create enhancement request for removing pgDevOps

	Hollys-MBP:bigsql hollyorr$ ./pgc status
	pgdevops running on port 8051
	Hollys-MBP:bigsql hollyorr$ ./pgc stop pgdevops
	pgdevops stopping
	Hollys-MBP:bigsql hollyorr$ ./pgc remove pgdevops
	pgdevops removing
	Hollys-MBP:bigsql hollyorr$ ls
	conf	data	hub	logs	pgc
	Hollys-MBP:bigsql hollyorr$ ./pgc status
	Hollys-MBP:bigsql hollyorr$ ./pgc info
	######################################################################
	#             PGC: v3.2.6  /Users/hollyorr/pgdg/bigsql
	#     User & Host: hollyorr  Hollys-MBP 192.168.1.9
	#        Platform: osx | Mac OS X 10.11.6
	#          Python: pip v9.0.1 | v2.7.10 | /usr/bin/python
	#        Hardware: 8 GB, 2 x Intel Core i5-3210M @ 2.50GHz
	#        Repo URL: http://10.0.1.118
	#     Last Update: 2017-07-27 13:35:28
	######################################################################
	Hollys-MBP:bigsql hollyorr$ ./pgc install pgdevops
	Hollys-MBP:bigsql hollyorr$ ./pgc start pgdevops
	Hollys-MBP:bigsql hollyorr$ ./pgc status
		pgdevops running on port 8051

Old connection information still remains. 

![](images/devops_enhancement.png)

Enhancement request: add flag:

	./pgc remove pgdevops --all

This would blow away pgdevops and all the sqllite data associated with it. 















